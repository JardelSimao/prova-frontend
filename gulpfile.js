var css = [
 	'./dev/styles/*.css'
];
var js  = [
    './dev/scripts/*.js'
];

var gulp = require('gulp');
var concat = require("gulp-concat");
var cssmin = require("gulp-cssmin");
var watch = require("gulp-watch");
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('minify-css', function(){
    gulp.src(css)
    .pipe(concat('style.min.css'))
    .pipe(cssmin())
    .pipe(gulp.dest('./assets/styles/'));
});

gulp.task('minify-js', function () {
    gulp.src(js) 
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./assets/scripts/'));
});

gulp.task('watch', function() {
    gulp.watch(js, ['minify-js']);
    gulp.watch(css, ['minify-css']);
});

gulp.task('browser-sync', function() {  
    browserSync.init(['./assets/styles/*', './assets/scripts/*','./index.html'], {
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('default',['minify-js','minify-css','watch','browser-sync']);