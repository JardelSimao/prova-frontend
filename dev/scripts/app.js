$(document).ready(function(){

	$("#dot_form").validate({
		rules:{
			nome:{
				required: true, minlength: 8
			},
			email:{
				required: true, email: true
			},
			mensagem:{
				required: true, minlength: 10
			}
		},
		messages:{
			nome:{
				required: "Digite o seu nome",
				minlength: jQuery.validator.format("O nome deve conter no mínimo {0} caracteres")
			},
			email:{
				required: "Digite o seu e-mail para contato",
				email: "Digite um e-mail válido"
			},
			mensagem:{
				required: "Digite a sua mensagem",
				minlength: jQuery.validator.format("{0} Caracteres são necessários para poder enviar a mensagem!")
			}
		}
	});

	$('input[name="telefone"]')
	.mask("(99) 9999-9999?9")
	.focusout(function (event) {  
		var target, phone, element;  
		target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
		phone = target.value.replace(/\D/g, '');
		element = $(target);  
		element.unmask();  
		if(phone.length > 10) {  
			element.mask("(99) 99999-999?9");  
		} else {  
			element.mask("(99) 9999-9999?9");  
		}  
	});

	var $carousel = $('.carousel');
	var $seats = $('.carousel-seat');

	$('.toggle').on('click', function(e) {
		var $newSeat;
		var $el = $('.is-ref');
		var $currSliderControl = $(e.currentTarget);

		$el.removeClass('is-ref');
		if ($currSliderControl.data('toggle') === 'next') {
			$newSeat = next($el);
			$carousel.removeClass('is-reversing');
		} else {
			$newSeat = prev($el);
			$carousel.addClass('is-reversing');
		}

		$newSeat.addClass('is-ref').css('order', 1);
		for (var i = 2; i <= $seats.length; i++) {
			$newSeat = next($newSeat).css('order', i);
		}

		$carousel.removeClass('is-set');
		return setTimeout(function() {
			return $carousel.addClass('is-set');
		}, 50);

		function next($el) {
			if ($el.next().length) {
				return $el.next();
			} else {
				return $seats.first();
			}
		}

		function prev($el) {
			if ($el.prev().length) {
				return $el.prev();
			} else {
				return $seats.last();
			}
		}
	});

	var acc = $(".accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
		}
	}
});